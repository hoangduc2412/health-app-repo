﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ComputerForSaleWebsite.BLL;
using System;
using System.Collections.Generic;
using System.Text;

namespace ComputerForSaleWebsite.BLL.Tests
{
    [TestClass()]
    public class ProductBllTests
    {
        [TestMethod()]
        public void GetProductsListTest()
        {
            Assert.IsTrue(new ProductBll().GetProductsList().Count == 0);
        }
    }
}