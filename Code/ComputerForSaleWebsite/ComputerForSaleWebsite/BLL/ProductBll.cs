﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ComputerForSaleWebsite.Models;
using Dapper;

namespace ComputerForSaleWebsite.BLL
{
    public class ProductBll : BaseBll
    {
        public List<ProductModel> GetProductsList()
        {
            return _dbContext.Query<ProductModel>("select * from product").ToList();
        }
    }
}
